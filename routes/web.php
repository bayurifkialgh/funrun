<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('/', App\Livewire\Web\Main::class)->name('home');

Route::get('pdf/voucher/{order_id}', [App\Http\Controllers\VoucherPDF::class, 'index'])->name('pdf-voucher');

Route::group([
    // 'auth'
    'middleware' => ['web', 'auth'],
    'prefix' => 'cms',
    'as' => 'cms.',
], function() {
    Route::get('/dashboard', App\Livewire\Cms\Dashboard::class)->name('dashboard');
    Route::get('/participant', App\Livewire\Cms\Participant::class)->name('participant');
    Route::get('/volunteer', App\Livewire\Cms\Volunter::class)->name('volunteer');
    Route::get('/event', App\Livewire\Cms\Event::class)->name('event');
    Route::get('/prize', App\Livewire\Cms\Prize::class)->name('prize');
    Route::get('/activity', App\Livewire\Cms\Activity::class)->name('activity');
    Route::get('/website', App\Livewire\Cms\Website::class)->name('website');
});

Route::fallback(function () {
    return view('errors.404');
});
