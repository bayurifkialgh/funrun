<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->string('email')->nullable();
            $table->string('phone', 15)->nullable();
            $table->text('address')->nullable();
            $table->string('author')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->text('opengraph')->nullable();
            $table->text('dulbincore')->nullable();
            $table->string('google_analytics')->nullable();
            $table->text('social_media')->nullable();
            $table->text('privacy_policy')->nullable();
            $table->text('term_of_service')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website');
    }
}
