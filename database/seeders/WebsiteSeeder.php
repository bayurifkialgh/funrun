<?php

namespace Database\Seeders;

use App\Models\Website;
use Illuminate\Database\Seeder;

class WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Website::truncate();
        Website::create([
            'name' => 'Laravel',
            'logo' => 'lgo.png',
            'favicon' => 'fav.png',
            'email' => 's5sL6@example.com',
            'phone' => '123456789',
            'address' => 'Jakarta',
            'description' => 'Laravel is a free, open-source PHP web framework.',
            'author' => 'Taylor Otwell',
            'keywords' => 'laravel, framework, php, web, development',
            'opengraph' => json_encode([
                'site_name' => 'Dulbin Core',
                'title' => 'Dulbin Core',
                'type' => 'website',
                'url' => 'https://dulbincore.com',
                'image' => 'https://dulbincore.com/logo.png',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum.',
            ]),
            'dulbincore' => json_encode([
                'title' => 'Dulbin Core',
                'publisher' => 'Dulbin Core',
                'publisher_url' => 'https://dulbincore.com',
                'creator_name' => 'Dulbin Core',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum.',
                'language' => 'id',
                'subject' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum.',
            ]),
            'google_analytics' => 'UA-123456789-1',
            'social_media' => json_encode([
                [
                    'name' => 'Facebook',
                    'icon' => 'bi bi-facebook',
                    'url' => 'https://facebook.com',
                ],
                [
                    'name' => 'Instagram',
                    'icon' => 'instagram',
                    'url' => 'https://instagram.com',
                ],
                [
                    'name' => 'Twitter',
                    'icon' => 'bi bi-twitter',
                    'url' => 'https://twitter.com',
                ],
                [
                    'name' => 'Youtube',
                    'icon' => 'bi bi-youtube',
                    'url' => 'https://youtube.com',
                ]
                ]),
                'privacy_policy' => 'Privacy Policy',
                'term_of_service' => 'Term of Service',
            ]);
    }
}
