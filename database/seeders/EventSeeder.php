<?php

namespace Database\Seeders;

use App\Models\Event;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::truncate();
        Event::create([
            'title' => 'The event',
            'subtitle' => 'The subtitle',
            'main_description' => 'The main description',
            'registration_description' => 'The registration description',
            'route' => 'The route',
            'route_description' => 'The route description',
            'activities_description' => 'The activities description',
            'registration_link' => 'The registration link',
            'event_details' => 'The event details',
            'start_date' => date('Y-m-d'),
            'start_hour' => date('H:i'),
            'end_hour' => date('H:i'),
            'location' => 'The location',
            'is_active' => true
        ]);
    }
}
