<x-app>
    <x-slot name="title">
        404 Not Fount
    </x-slot>

    <div class="container mt-5 py-5 text-center">
        <img src="{{ asset('404.svg') }}" alt="Not Found" class="img-fluid">
        <h1>Not Found</h1>
        <a href="{{ route('home') }}" class="btn btn-primary">
            <span class="icon-home"></span> Go Back
        </a>
    </div>

    @push('scripts')
        <script>
            $('.nav-link').click(function() {
                location.href = `{{ route('home' ) }}/${$(this).attr('href')}`
            })
        </script>
    @endpush
</x-app>
