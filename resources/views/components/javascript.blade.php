<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
<script>
    document.addEventListener('livewire:load', () => {
        // Toast initialization
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Livewire.on('alert', params => {
            Toast.fire({
                icon: params.type,
                title: params.message,
            })
        })

        Livewire.on('confirm', params => {
            Swal.fire({
                title: params.title ?? 'Are you sure?',
                text: params.message ?? `You won't be able to revert this`,
                icon: params.icon ?? 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emit(params.function, params.id)
                }
            })
        })

        Livewire.hook('element.updated', (el, component) => {
            feather.replace()
        })


        Livewire.on('closeModal', params => {
            const modal = document.getElementById(params)
            const modalBackdrop = document.getElementsByClassName('modal-backdrop')[0]

            modal?.classList?.remove('show')
            modalBackdrop?.remove()
        })
    })
</script>
