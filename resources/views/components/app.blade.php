<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{ $meta ?? '' }}


    <link href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600, 700,900|Oswald:400,700" rel="stylesheet">


    <link rel="stylesheet" href="{{ asset('fe/fonts/icomoon/style.css') }}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <link rel="stylesheet" href="{{ asset('fe/fonts/flaticon/font/flaticon.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>

    <link rel="stylesheet" href="{{ asset('fe/css/style.css') }}">
    @livewireStyles
    <title>{{ $title ?? '' }}</title>
    <style>
        :root {
            --primary-color: #F2305F;
            /* --background-color: #F5EEDC; */
            /* --background-color: #ebd5bA48; */
            /* --background-color: #e5d475; */
            --background-color: #F9E247;
        }

        html {
            background: var(--background-color);
        }

        body {
            background: var(--background-color);
            font-family:'Poppins', sans-serif;
            overflow-x: hidden !important;
        }
        .heading {
            font-weight: 700 !important;
            text-align: center !important;
        }
        .header-bar {
            background: var(--background-color);
        }

        .btn-outline-primary {
            border-color: var(--primary-color);
            color: white;
        }

        .btn-primary {
            border-color: var(--primary-color);
            background: var(--primary-color);
            color: white;
        }

        .btn-primary:hover {
            border-color: var(--primary-color);
            background: var(--primary-color);
            color: white;
        }

        .btn-outline-primary:hover {
            border-color: var(--primary-color);
            background: var(--primary-color);
            color: white;
        }

        .text-primary {
            color: var(--primary-color) !important;
        }

        .location {
            position: absolute;
            bottom: 4px;
            left: 0;
            width: 100%;
            z-index: 99999;
        }
    </style>
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="200">


    <div class="site-wrap">

        <div class="site-mobile-menu site-navbar-target">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>

        <header class="header-bar d-flex d-lg-block align-items-center site-navbar-target" data-aos="fade-right">
            <div class="site-logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('logo.png') }}" alt="{{ $header_name ?? '' }}" class="img-fluid">
                </a>
            </div>

            <div class="d-inline-block d-lg-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a
                    href="#" class="site-menu-toggle js-menu-toggle "><span class="icon-menu h3"></span></a></div>

            <div class="main-menu">
                <ul class="js-clone-nav">
                    <li><a href="#section-home" class="nav-link">Home</a></li>
                    <li><a href="#section-event" class="nav-link">Our Mission</a></li>
                    <li><a href="#section-activity" class="nav-link">Event Schedule</a></li>
                    <li><a href="#section-prize" class="nav-link">Door Prize</a></li>
                    <li><a href="#section-route" class="nav-link">Route</a></li>
                    <li><a href="#section-register" class="nav-link">Register</a></li>
                    <li><a href="#section-volunteer" class="nav-link">Become a Volunteer</a></li>
                    <li><a href="#section-about-us" class="nav-link">About Us</a></li>
                    <li><a href="#section-information" class="nav-link">Contact Us</a></li>
                </ul>
                <ul class="social js-clone-nav">
                    <li><a href="#"><span class="icon-instagram"></span></a></li>
                    <li><a href="#"><span class="icon-facebook"></span></a></li>
                    <li><a href="#"><span class="icon-twitter"></span></a></li>
                    <li><a href="#"><span class="icon-linkedin"></span></a></li>
                </ul>
            </div>
        </header>
        <main class="main-content">
            {{ $slot ?? '' }}
        </main>

    </div> <!-- .site-wrap -->

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stellar.js/0.6.1/jquery.stellar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

    <script src="{{ asset('fe/js/main.js') }}"></script>

    @livewireScripts

    @if(config('midtrans.is_production'))
        <script type="text/javascript" src="https://app.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}"></script>
    @else
        <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}"></script>
    @endif

    @stack('scripts')
</body>

</html>
