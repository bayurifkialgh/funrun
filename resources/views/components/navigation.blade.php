<div>
    <nav id="sidebar" class="sidebar js-sidebar">
        <div class="sidebar-content js-simplebar">
            <a class="sidebar-brand" href="{{ route('cms.dashboard') }}" wire:navigate>
                <span class="align-middle">GGWP</span>
            </a>

            <ul class="sidebar-nav">
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.dashboard') }}">
                        <i class="align-middle" data-feather="home"></i>
                        <span class="align-middle">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.participant') }}">
                        <i class="align-middle" data-feather="users"></i>
                        <span class="align-middle">Participant</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.volunteer') }}">
                        <i class="align-middle" data-feather="users"></i>
                        <span class="align-middle">Volunteer</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.event') }}">
                        <i class="align-middle" data-feather="edit"></i>
                        <span class="align-middle">Event</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.prize') }}">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                        <span class="align-middle">Prize</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.activity') }}">
                        <i class="align-middle" data-feather="activity"></i>
                        <span class="align-middle">Activities</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{ route('cms.website') }}">
                        <i class="align-middle" data-feather="settings"></i>
                        <span class="align-middle">Website</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
