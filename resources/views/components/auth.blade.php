<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <link href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600, 700,900|Oswald:400,700" rel="stylesheet">


    <link rel="stylesheet" href="{{ asset('fe/fonts/icomoon/style.css') }}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('fe/css/style.css') }}">
    @livewireStyles
    <title>LOGIN</title>
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="200">


    <div class="site-wrap">
        <main class="mt-5">
            {{ $slot ?? '' }}
        </main>

    </div> <!-- .site-wrap -->
    @livewireScripts
</body>

</html>
