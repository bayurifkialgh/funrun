<div>
    <x-slot name="title">
        {{ $settings->name }}
    </x-slot>

    <x-slot name="header_name">
        {{ $settings->name }}
    </x-slot>

    <x-slot name="meta">
        {{-- <link rel="preconnect" href="https://fonts.gstatic.com"> --}}
        <meta content="{{ $settings->description }}" name="description">
        <meta content="{{ $settings->keywords }}" name="keywords">
        <meta content="{{ $settings->author }}" name="author">

        {{-- Opengraph --}}
        @foreach($settings->opengraph as $key => $val)
            @php
                $additional = '';
                if($key == 'image') {
                    $additional = 'itemprop=image';
                    $val = asset('storage/settings/' . $val);
                }
            @endphp
            <meta property="og:{{ $key }}" {{ $additional }} content="{{ $val }}">
        @endforeach

        {{-- Dulbincode --}}
        <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
        @foreach($settings->dulbincore as $key => $val)
            @php
                $key = str_replace('_', '.', $key);
            @endphp
            <meta name="DC:{{ $key }}" content="{{ $val }}">
        @endforeach

        <!-- Favicons -->
        <link rel="shortcut" href="{{ asset('storage/settings/' . $settings->favicon) }}">
        <link rel="icon" sizes="32x32" href="{{ asset('storage/settings/' . $settings->favicon) }}">
        <link rel="icon" sizes="16x16" href="{{ asset('storage/settings/' . $settings->favicon) }}">
        <link href="{{ asset('storage/settings/' . $settings->favicon) }}" sizes="180x180" rel="apple-touch-icon">
    </x-slot>

    <livewire:web.components.home :event="$event" />

    <div class="container-fluid site-section">

        <livewire:web.components.event :event="$event" :settings="$settings" />

        <livewire:web.components.activity />

        <livewire:web.components.prize />

        <livewire:web.components.route :event="$event" />

        <livewire:web.components.register />

        <livewire:web.components.volunter />

        <livewire:web.components.about-us :settings="$settings" />

        <livewire:web.components.information :settings="$settings" />

        <div class="row justify-content-center">
            <div class="col-md-12 text-center py-5">
                <p>{{ $settings->name }} Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script>. All Rights Reserved.
                </p>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            Livewire.on('payment', (snapToken) => {
                snap.pay(snapToken, {
                    // Optional
                    onSuccess: function(result){
                        toastr.success('Pembayaran sukses');
                    },
                    // Optional
                    onPending: function(result){
                        toastr.warning('Pembayaran sedang diproses');
                    },
                    // Optional
                    onError: function(result){
                        toastr.error('Pembayaran gagal');
                    }
                });
            });

            Livewire.on('alert', params => {
                toastr[params.type](params.message);
            })

            Livewire.on('closeModal', id => {
                $(`#${id}`).modal('hide')
            })
        </script>
    @endpush
</div>
