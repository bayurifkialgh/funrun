<div>
    <section class="site-section" id="section-volunteer">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @php
                        $syarat_ketentuan = 'https://drive.google.com/file/d/1wpdldr_yuWrozXGzrP_jT8z7YkIrt3Zi/view?usp=sharing';
                    @endphp
                    <h2 class=" mb-5 heading">Become a Volunteer</h2>
                    <p>
                        Bergabung menjadi volunteer dan dapatkan:
                    </p>
                    <div class="d-block mt-5">
                        <div class="mr-md-auto mr-2">
                            <ul class="ul-check list-unstyled primary">
                                <li>
                                    <h6>Kaos Panitia</h6>
                                </li>
                                <li>
                                    <h6>Goodie Bag</h6>
                                </li>
                                <li>
                                    <h6>Konsumsi</h6>
                                </li>
                                <li>
                                    <h6>Teman dan Relasi Baru</h6>
                                </li>
                                <a href="{{ $syarat_ketentuan ?? '' }}" target="_blank">Syarat & Ketentuan</a>
                            </ul>
                        </div>
                    </div>
                    <h6 class="text-danger mb-4">
                        Anda akan bisa langsung masuk grup volunteer setelah mendaftar.
                    </h6>
                    @if(session('volunter'))
                        <form wire:submit.prevent="save">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="" for="name">Nama Lengkap</label>
                                    <input type="text" wire:model.defer="name" id="name" class="form-control" readonly>
                                    <x-acc-input-error for="name" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label class="" for="email">Email</label>
                                    <input type="email" wire:model.defer="email" id="email" class="form-control" readonly>
                                    <x-acc-input-error for="email" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label class="" for="phone">Nomer HP</label>
                                    <input type="number" wire:model.defer="phone" id="phone" class="form-control" readonly>
                                    <x-acc-input-error for="phone" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <button type="button" wire:click="new" class="btn btn-warning btn-md">
                                        Daftar Baru
                                    </button>
                                    <button href="https://chat.whatsapp.com/KYGwq1vTv4yGMEN3UFWMYP" target="_blank" class="btn btn-success btn-md">
                                        Masuk Grup WhatsApp
                                    </button>
                                </div>
                            </div>

                        </form>
                    @else
                        <form wire:submit.prevent="save">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="" for="name">Nama Lengkap</label>
                                    <input type="text" wire:model.defer="name" id="name" class="form-control">
                                    <x-acc-input-error for="name" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label class="" for="email">Email</label>
                                    <input type="email" wire:model.defer="email" id="email" class="form-control">
                                    <x-acc-input-error for="email" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label class="" for="phone">Nomer HP</label>
                                    <input type="number" wire:model.defer="phone" id="phone" class="form-control">
                                    <x-acc-input-error for="phone" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md">
                                        Daftar
                                    </button>

                                </div>
                            </div>

                        </form>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
