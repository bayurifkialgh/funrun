<div>
    <section class="site-section" id="section-activity">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h2 class="heading">Event Schedule</h2>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <p></p>

                        <div class="d-block mt-5">
                            <div class="mr-md-auto mr-2">
                                <ul class="ul-check list-unstyled primary">
                                    @foreach($activities as $ac)
                                        <li>
                                            <h6>{!! $ac->title !!}</h6>
                                            <p>
                                                {!! $ac->description !!}
                                            </p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
