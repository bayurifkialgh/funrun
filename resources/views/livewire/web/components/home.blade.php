<div>
    <section
        class="site-section-hero bg-image mb-5"
        style="background-image: url('{{ asset('fe/banner.png') }}');"
        data-stellar-background-ratio="0.5"
        id="section-home">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-7 text-center">
                <h2 class="text-uppercase text-white" data-aos="fade-up">{!! $event->title !!}</h4>
                <p data-aos="fade-up" data-aos-delay="100">
                    <a href="{{ $event->registration_link }}" class="btn btn-outline-primary btn-md smoothscroll">
                        {{ $event->registration_description }}
                    </a>
                    <br>
                    <br>
                </p>
            </div>
        </div>
        <div class="location">
            <p class="lead text-white text-center mb-4" data-aos="fade-up" data-aos-delay="100">
                {{ $event->start_date->isoFormat('dddd, D MMMM Y') }}
                <br>{{ $event->start_hour->format('H:i') }} - {{ $event->end_hour->format('H:i') }}
                <br>{!! $event->location !!}
                <br>
                <br>{!! $event->price_total !!}
            </p>
        </div>
    </section>
</div>
