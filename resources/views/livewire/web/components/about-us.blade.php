<div>
    <section class="site-section" id="section-about-us">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-md-8">

                    <h2 class="heading">About Us</h2>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <p>{!! $settings->about_us !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
