<div>
    <section class="site-section" id="section-route">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-md-8">

                    <h2 class="heading">Route</h2>
                    <div data-aos="fade-up" data-aos-delay="100" class="text-center mb-5">
                        <img src="{{ asset('storage/events/' . $event->map) }}" alt="Map Route" class="img-fluid">
                    </div>

                    <div data-aos="fade-up" data-aos-delay="100" class="text-center mb-5">
                        <img src="{{ asset('storage/events/' . $event->map2) }}" alt="Map Route" class="img-fluid">
                    </div>

                    <div class="mt-5 align-item-center table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="border-color: black !important">Juara</th>
                                    <th style="border-color: black !important" colspan="2">10k</th>
                                    <th style="border-color: black !important" colspan="2">5k</th>
                                </tr>
                                <tr>
                                    <th style="border-color: black !important"></th>
                                    <th style="border-color: black !important">Male</th>
                                    <th style="border-color: black !important">Female</th>
                                    <th style="border-color: black !important">Male</th>
                                    <th style="border-color: black !important">Female</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="border-color: black !important">1</td>
                                    <td style="border-color: black !important">Rp 1.500.000</td>
                                    <td style="border-color: black !important">Rp 1.500.000</td>
                                    <td style="border-color: black !important">Rp 1.000.000</td>
                                    <td style="border-color: black !important">Rp 1.000.000</td>
                                </tr>
                                <tr>
                                    <td style="border-color: black !important">2</td>
                                    <td style="border-color: black !important">Rp 1.000.000</td>
                                    <td style="border-color: black !important">Rp 1.000.000</td>
                                    <td style="border-color: black !important">Rp 750.000</td>
                                    <td style="border-color: black !important">Rp 750.000</td>
                                </tr>
                                <tr>
                                    <td style="border-color: black !important">3</td>
                                    <td style="border-color: black !important">Rp 750.000</td>
                                    <td style="border-color: black !important">Rp 750.000</td>
                                    <td style="border-color: black !important">Rp 500.000</td>
                                    <td style="border-color: black !important">Rp 500.000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
