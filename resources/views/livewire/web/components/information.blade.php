<div>
    <section class="site-section" id="section-information">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-md-8">

                    <h2 class="heading">Contact Us</h2>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <div class="d-block mt-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>
                                        <span class="icon-phone"></span> Telepon
                                    </h4>
                                    <a href="http://wa.me/{{ $settings->phone }}?text=" class="mb-4" target="_blank">{{ $settings->phone }}</a>
                                </div>
                                <div class="col-md-4">
                                    <h4>
                                        <span class="icon-envelope"></span> Email
                                    </h4>
                                    <a href="mailto:{{ $settings->email }}" class="mb-4">{{ $settings->email }}</a>
                                </div>
                                <div class="col-md-4">
                                    <h4>
                                        <span class="icon-instagram"></span> Instagram
                                    </h4>
                                    <a href="https://www.instagram.com/{{ $settings->address }}" class="mb-4">{{ '@' . $settings->address }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
