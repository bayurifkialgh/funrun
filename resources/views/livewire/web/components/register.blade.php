<div>
    <section class="site-section" id="section-register">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h2 class=" mb-5 heading">Register</h2>
                    <h6 class="text-danger mb-4">
                        Voucher akan dikirimkan ke email atau bisa juga download pdf dibawah setelah anda mengisi data
                        dibawah ini
                        <br>
                        <br>
                        (Cek juga folder SPAM email anda)
                    </h6>
                    @php
                        $syarat_ketentuan = 'https://drive.google.com/file/d/1U3zgbwEn7n_wdsM6u2G3mqetM4SAb5qh/view?usp=sharing';
                    @endphp
                    @if (session('participant'))
                        <form wire:submit.prevent="save">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="jenis_lari">Jenis Lari</label>
                                    <select wire:model="jenis_lari" id="jenis_lari" class="form-control" readonly>
                                        <option value="">--Jenis Lari--</option>
                                        <option value="Grup">Grup</option>
                                        <option value="Perorangan">Perorangan</option>
                                    </select>
                                    <x-acc-input-error for="jenis_lari" />
                                </div>
                            </div>

                            @if ($jenis_lari == 'Grup')
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label style="font-weight: 600" class="" for="grup">Grup /
                                            Circle</label>
                                        <input type="text" wire:model="grup" id="grup" class="form-control"
                                            readonly>
                                        <x-acc-input-error for="grup" />
                                    </div>
                                </div>
                            @endif

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="name">Nama Lengkap</label>
                                    <input type="text" wire:model.defer="name" id="name" class="form-control"
                                        readonly>
                                    <x-acc-input-error for="name" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="email">Email</label>
                                    <input type="email" wire:model.defer="email" id="email" class="form-control"
                                        readonly>
                                    <x-acc-input-error for="email" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="phone">Nomer HP</label>
                                    <input type="number" wire:model.defer="phone" id="phone" class="form-control"
                                        readonly>
                                    <x-acc-input-error for="phone" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="birth_date">Tanggal
                                        Lahir</label>
                                    <input type="date" wire:model.defer="birth_date" id="birth_date"
                                        class="form-control" readonly>
                                    <x-acc-input-error for="birth_date" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="gender">Jenis Kelamin</label>
                                    <select id="gender" wire:model.defer="gender" class="form-control" disabled>
                                        <option value="">-- Pilih Jenis Kelamin --</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                    <x-acc-input-error for="gender" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="address">Domisili</label>
                                    <textarea id="address" wire:model.defer="address" class="form-control" readonly></textarea>
                                    <x-acc-input-error for="address" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="distance">Nomor Jarak</label>
                                    <select id="distance" wire:model.defer="distance" class="form-control" disabled>
                                        <option value="">-- Nomor --</option>
                                        <option value="5k">5K (200.000) </option>
                                        <option value="10k">10K (300.000)</option>
                                    </select>
                                    <x-acc-input-error for="distance" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <p>
                                        <input type="checkbox" checked>
                                        Saya sudah membaca
                                        <span><a href="{{ $syarat_ketentuan ?? '' }}" target="_blank">Syarat &
                                                Ketentuan</a></span>
                                        Yang berlaku
                                    </p>
                                    <x-acc-input-error for="syarat" />

                                    <h6 class="text-danger mb-4">
                                        <button class="btn btn-{{ $recent->status == 1 ? 'success' : 'danger' }}">
                                            @if($recent->status == 1)
                                                Tervalidasi
                                            @elseif($recent->status == 2)
                                                Ditolak
                                            @else
                                                Sedang Dicek
                                            @endif
                                        </button>
                                        @if($recent->status == 1)
                                            <br>
                                            <br>
                                            <a href="{{ route('pdf-voucher', ['order_id' => $recent->order_id]) }}" target="_blank">Lihat Voucher</a>
                                        @endif
                                    </h6>

                                    {{-- <button type="button" wire:click="$emit('payment', '{{ $snapToken }}')" class="btn btn-primary btn-md">
                                        Bayar
                                    </button> --}}

                                    <button type="button" data-toggle="modal" data-target="#modal-bukti-pembayaran" class="btn btn-primary btn-md">
                                        Bayar
                                    </button>

                                    <button type="button" wire:click="new" class="btn btn-warning btn-md">
                                        Daftar Baru
                                    </button>
                                </div>
                            </div>

                        </form>
                    @else
                        <form wire:submit.prevent="save">

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="jenis_lari">Jenis
                                        Lari</label>
                                    <select wire:model="jenis_lari" id="jenis_lari" class="form-control">
                                        <option value="">--Jenis Lari--</option>
                                        <option value="Grup">Grup</option>
                                        <option value="Perorangan">Perorangan</option>
                                    </select>
                                    <x-acc-input-error for="jenis_lari" />
                                </div>
                            </div>

                            @if ($jenis_lari == 'Grup')
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label style="font-weight: 600" class="" for="grup">Grup /
                                            Circle</label>
                                        <input type="text" wire:model="grup" id="grup" class="form-control">
                                        <x-acc-input-error for="grup" />
                                    </div>
                                </div>
                            @endif

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="name">Nama
                                        Lengkap</label>
                                    <input type="text" wire:model.defer="name" id="name"
                                        class="form-control">
                                    <x-acc-input-error for="name" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="email">Email</label>
                                    <input type="email" wire:model.defer="email" id="email"
                                        class="form-control">
                                    <x-acc-input-error for="email" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="phone">Nomer HP</label>
                                    <input type="number" wire:model.defer="phone" id="phone"
                                        class="form-control">
                                    <x-acc-input-error for="phone" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="birth_date">Tanggal
                                        Lahir</label>
                                    <input type="date" wire:model.defer="birth_date" id="birth_date"
                                        class="form-control">
                                    <x-acc-input-error for="birth_date" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="gender">Jenis
                                        Kelamin</label>
                                    <select id="gender" wire:model.defer="gender" class="form-control">
                                        <option value="">-- Pilih Jenis Kelamin --</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                    <x-acc-input-error for="gender" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="address">Domisili</label>
                                    <textarea id="address" wire:model.defer="address" class="form-control"></textarea>
                                    <x-acc-input-error for="address" />
                                </div>
                            </div>

                            <div class="row form-group">

                                <div class="col-md-12">
                                    <label style="font-weight: 600" class="" for="distance">Nomor Jarak</label>
                                    <select id="distance" wire:model.defer="distance" class="form-control">
                                        <option value="">-- Nomor --</option>
                                        <option value="5k">5K (200.000) </option>
                                        <option value="10k">10K (300.000)</option>
                                    </select>
                                    <x-acc-input-error for="distance" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <p>
                                        <input type="checkbox" wire:model="syarat" value="1">
                                        Saya sudah membaca
                                        <span><a href="{{ $syarat_ketentuan ?? '' }}" target="_blank">Syarat &
                                                Ketentuan</a></span>
                                        Yang berlaku
                                    </p>
                                    <x-acc-input-error for="syarat" />
                                    <button type="submit" class="btn btn-primary btn-md">
                                        Daftar
                                    </button>
                                </div>
                            </div>

                        </form>
                    @endif
                </div>
            </div>
        </div>
    </section>

    {{-- Modal --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div class="modal fade" id="modal-bukti-pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true" wire:ignore.self>
        <div class="modal-dialog modal-lg" role="document">
            <form wire:submit.prevent="upload">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">
                            Pembayaran dilakukan ke Nomer Rekening Mandiri di bawah ini<br>
                            <b>BUFF Indonesia Bersatu</b><br>
                            <button class="btn btn-primary" x-data @click="navigator.clipboard.writeText('130 00 2384075 7')">
                                <b>
                                    <i class="fa fa-copy"></i>
                                    130 00 2384075 7
                                </b>
                            </button>
                        </p>
                        <p class="text-center">
                            @php
                                $price = $distance == '5k' ? '200000' : '300000';
                                $price = substr_replace($price, $final_number, $distance == '5k' ? -4 : -5);
                            @endphp
                            {{ number_format($price) }}
                        </p>
                        <div class="form-group">
                            @if($recent)
                                <img src="{{ !is_string($file) && $file ? $file?->temporaryUrl() : url('storage/events/' . $recent?->bukti_pembayaran ?? '') }}" alt="Prize" class="img-fluid">
                            @endif
                            <label for="">Bukti Pembayaran</label>
                            <input type="file" class="form-control" wire:model="file">
                            <x-acc-input-error for="file" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
