<div>
    <section class="site-section" id="section-event">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-md-8">

                    <h2 class="heading">Our Mission</h2>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <div class="text-center mb-3">
                            <img
                                src="{{ asset('storage/settings/' . $settings->logo) }}"
                                alt="{{ $settings->name }}"
                                class="img-fluid"
                                style="height: 300px">
                        </div>
                        <p>{!! $event->main_description !!}</p>

                        {{-- <div class="d-block d-md-flex mt-5">
                            <div class="mr-md-auto mr-2">
                                <ul class="ul-check list-unstyled primary">
                                    <li>Optio eveniet ex laborum</li>
                                    <li>Inventore sapiente tenetur</li>
                                    <li>Ipsam aliquam esse</li>
                                </ul>
                            </div>
                            <div class="mr-md-auto">
                                <ul class="ul-check list-unstyled primary">
                                    <li>Optio eveniet ex laborum</li>
                                    <li>Inventore sapiente tenetur</li>
                                    <li>Ipsam aliquam esse</li>
                                </ul>
                            </div>
                        </div> --}}

                        {{-- Map --}}

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
