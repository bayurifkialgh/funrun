<div>
    <section class="site-section" id="section-prize">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-md-8">

                    <h2 class="heading">Door Prize</h2>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <p></p>
                        <div class="d-block mt-5">
                            <div class="mr-md-auto mr-2">
                                <div class="row">
                                    @foreach($prize as $p)
                                        <div class="col-md-4 col-6">
                                            <h6>{!! $p->name !!}</h6>
                                            <img src="{{ asset('storage/prizes/' . $p->image) }}" alt="{{ $p->name }}" class="img-fluid">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
