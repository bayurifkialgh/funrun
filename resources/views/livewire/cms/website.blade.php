<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>

    <h1 class="h3 mb-3">
        {{ $title ?? '' }}
    </h1>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">{{ $title ?? '' }} Data</h5>
        </div>
        <div class="card-body">
            <x-acc-form submit="save">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Google Analytic</label>
                        <input type="text" wire:model="google_analytics" class="form-control" placeholder="Google Analytic">
                        <x-acc-input-error for="google_analytics" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">About Us</label>
                        <textarea wire:model="about_us" class="form-control" placeholder="About Us"></textarea>
                        <x-acc-input-error for="about_us" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">App Name</label>
                        <input type="text" wire:model="m_name" class="form-control" placeholder="App Name">
                        <x-acc-input-error for="m_name" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">App Logo</label>
                        @if ($data->logo)
                            <div class="mb-3">
                                <img src="{{ $m_logo ? $m_logo->temporaryUrl() : url('storage/settings/' . $data->logo) }}" alt="logo" class="img-fluid">
                            </div>
                        @endif
                        <input type="file" wire:model="m_logo" class="form-control">
                        <x-acc-input-error for="m_logo" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">App Icon</label>
                        @if ($data->favicon)
                            <div class="mb-3">
                                <img src="{{ $m_favicon ? $m_favicon->temporaryUrl() : url('storage/settings/' . $data->favicon) }}" alt="favicon" class="img-fluid">
                            </div>
                        @endif
                        <input type="file" wire:model="m_favicon" class="form-control">
                        <x-acc-input-error for="m_favicon" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="text" wire:model="m_email" class="form-control" placeholder="Email">
                        <x-acc-input-error for="m_email" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Phone</label>
                        <input type="number" wire:model="m_phone" class="form-control" placeholder="Phone">
                        <x-acc-input-error for="m_phone" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Instagram</label>
                        <input type="text" wire:model="m_address" class="form-control" placeholder="Instagram">
                        <x-acc-input-error for="m_address" />
                    </div>
                </div>

                {{-- Meta Tag --}}
                <div class="col-md-12">
                    <hr>
                    <div class="text-center">
                        <h1 class="h-2 mb-3">Meta Tag</h1>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Website Author</label>
                        <input type="text" wire:model="m_author" class="form-control" placeholder="Author">
                        <x-acc-input-error for="m_author" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Website Description</label>
                        <textarea wire:model="m_description" class="form-control" placeholder="Description"></textarea>
                        <x-acc-input-error for="m_description" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Website Keywords</label>
                        <textarea wire:model="m_keywords" class="form-control" placeholder="Keywords"></textarea>
                        <x-acc-input-error for="m_keywords" />
                    </div>
                </div>

                {{-- Open Graph --}}
                <div class="col-md-12">
                    <hr>
                    <div class="text-center">
                        <h1 class="h-2 mb-3">Open Graph Tag</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Site name</label>
                        <input type="text" wire:model="m_opengraph.site_name" class="form-control" placeholder="Site Name">
                        <x-acc-input-error for="m_opengraph.site_name" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Title</label>
                        <input type="text" wire:model="m_opengraph.title" class="form-control" placeholder="Title">
                        <x-acc-input-error for="m_opengraph.title" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Type</label>
                        <input type="text" wire:model="m_opengraph.type" class="form-control" placeholder="Type">
                        <x-acc-input-error for="m_opengraph.type" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Image</label>
                        @if ($opengraph_image)
                            <div class="mb-3">
                                <img src="{{ $m_opengraph['image'] ? $m_opengraph['image']->temporaryUrl() : url('storage/settings/' . $opengraph_image) }}" alt="open graph image" class="img-fluid">
                            </div>
                        @endif
                        <input type="file" wire:model="m_opengraph.image" class="form-control">
                        <x-acc-input-error for="m_opengraph.image" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">URL</label>
                        <input type="text" wire:model="m_opengraph.url" class="form-control" placeholder="URL">
                        <x-acc-input-error for="m_opengraph.url" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <textarea wire:model="m_opengraph.description" class="form-control" placeholder="Description"></textarea>
                        <x-acc-input-error for="m_opengraph.description" />
                    </div>
                </div>

                {{-- Dulbin Core --}}
                <div class="col-md-12">
                    <hr>
                    <div class="text-center">
                        <h1 class="h-2 mb-3">Dulbin Core Tag</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Title</label>
                        <input type="text" wire:model="m_dulbincore.title" class="form-control" placeholder="Title">
                        <x-acc-input-error for="m_dulbincore.title" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Publisher</label>
                        <input type="text" wire:model="m_dulbincore.publisher" class="form-control" placeholder="Publisher">
                        <x-acc-input-error for="m_dulbincore.publisher" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Publisher URL</label>
                        <input type="text" wire:model="m_dulbincore.publisher_url" class="form-control" placeholder="Publisher URL">
                        <x-acc-input-error for="m_dulbincore.publisher_url" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <textarea wire:model="m_dulbincore.description" class="form-control" placeholder="Description"></textarea>
                        <x-acc-input-error for="m_dulbincore.description" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Creator</label>
                        <input type="text" wire:model="m_dulbincore.creator_name" class="form-control" placeholder="Creator">
                        <x-acc-input-error for="m_dulbincore.creator_name" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Subject</label>
                        <input type="text" wire:model="m_dulbincore.subject" class="form-control" placeholder="Subject">
                        <x-acc-input-error for="m_dulbincore.subject" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Language</label>
                        <input type="text" wire:model="m_dulbincore.language" class="form-control" placeholder="Language">
                        <x-acc-input-error for="m_dulbincore.language" />
                    </div>
                </div>
                <x-slot name="actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-end">
                                <button
                                    class="btn btn-success"
                                    wire:loading.attr="disabled"
                                    wire:target="save"
                                    type="submit">
                                    <i class="align-middle" data-feather="save"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-acc-form>
        </div>
    </div>
</div>
