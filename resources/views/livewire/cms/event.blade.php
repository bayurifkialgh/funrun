<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>

    <h1 class="h3 mb-3">
        {{ $title ?? '' }}
    </h1>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">{{ $title ?? '' }} Data</h5>
        </div>
        <div class="card-body">
            <x-acc-form submit="save">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Title</label>
                        <input type="text" wire:model="m_title" class="form-control" placeholder="Title">
                        <x-acc-input-error for="m_title" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Sub Title</label>
                        <input type="text" wire:model="m_subtitle" class="form-control" placeholder="Sub Title">
                        <x-acc-input-error for="m_subtitle" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <textarea wire:model="m_main_description" class="form-control" placeholder="Main Description" rows="5"></textarea>
                        <x-acc-input-error for="m_main_description" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Registration Text</label>
                        <input type="text" wire:model="m_registration_description" class="form-control" placeholder="Registration Description">
                        <x-acc-input-error for="m_registration_description" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Registration Link</label>
                        <input type="text" wire:model="m_registration_link" class="form-control" placeholder="Registration Link">
                        <x-acc-input-error for="m_registration_link" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Start Date</label>
                        <input type="date" wire:model="m_start_date" class="form-control">
                        <x-acc-input-error for="m_start_date" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Start Hour</label>
                        <input type="time" wire:model="m_start_hour" class="form-control">
                        <x-acc-input-error for="m_start_hour" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">End Hour</label>
                        <input type="time" wire:model="m_end_hour" class="form-control">
                        <x-acc-input-error for="m_end_hour" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Total Price</label>
                        <input type="text" wire:model="m_price_total" class="form-control">
                        <x-acc-input-error for="m_price_total" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Location</label>
                        <textarea wire:model="m_location" class="form-control" placeholder="Location"></textarea>
                        <x-acc-input-error for="m_location" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Map 5k</label>
                        <div class="mb-3">
                            <img src="{{ is_string($m_map) ? $m_map->temporaryUrl() : url('storage/events/' . $data->map) }}" alt="Prize" class="img-fluid">
                        </div>
                        <input type="file" wire:model="m_map" class="form-control">
                        <x-acc-input-error for="m_map" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Map 10k</label>
                        <div class="mb-3">
                            <img src="{{ is_string($m_map2) ? $m_map2->temporaryUrl() : url('storage/events/' . $data->map2) }}" alt="Prize" class="img-fluid">
                        </div>
                        <input type="file" wire:model="m_map2" class="form-control">
                        <x-acc-input-error for="m_map2" />
                    </div>
                </div>
                <x-slot name="actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-end">
                                <button
                                    class="btn btn-success"
                                    wire:loading.attr="disabled"
                                    wire:target="save"
                                    type="submit">
                                    <i class="align-middle" data-feather="save"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-acc-form>
        </div>
    </div>
</div>
