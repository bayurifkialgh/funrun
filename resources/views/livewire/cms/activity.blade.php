<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>

    <h1 class="h3 mb-3">
        {{ $title ?? '' }}
    </h1>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">{{ $title ?? '' }} Data</h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <x-acc-header />
                <table class="table table-hover table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <x-acc-loop-th :searchBy="$searchBy" :orderBy="$orderBy" :order="$order" />
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($get as $d)
                            <tr>
                                <td>{{ $d->order }}</td>
                                <td>{{ $d->title }}</td>
                                <td>{{ $d->description }}</td>
                                <td>
                                    <button
                                        x-data
                                        class="btn btn-primary"
                                        wire:loading.attr="disabled"
                                        wire:target="edit"
                                        wire:click="edit({{ $d->id }})"
                                        @click="new bootstrap.Modal(document.getElementById('acc-modal')).show()">
                                        <i class="align-middle" data-feather="edit"></i>
                                    </button>
                                    <button
                                        class="btn btn-danger"
                                        wire:click="confirmDelete({{ $d->id }})"
                                    >
                                        <i class="align-middle" data-feather="trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100" class="text-center">
                                    No Data Found
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                <div class="float-end">
                    {{ $get->links() }}
                </div>
            </div>
        </div>
    </div>

    <x-acc-modal title="Show {{ $title }}" id="acc-modal">
        <x-acc-form submit="save">
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Order</label>
                    <input type="number" wire:model="m_order" class="form-control" >
                    <x-acc-input-error for="m_order" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Title</label>
                    <input type="text" wire:model="m_title" class="form-control" >
                    <x-acc-input-error for="m_title" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Description</label>
                    <textarea wire:model="m_description" class="form-control" ></textarea>
                    <x-acc-input-error for="m_description" />
                </div>
            </div>
        </x-acc-form>
    </x-acc-modal>
</div>
