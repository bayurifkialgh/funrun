<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>

    <div class="container">
        <div class="row">
            <div class="col-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            Participants
                        </h5>
                    </div>
                    <div class="card-body">
                        <h1 class="card-text text-center">
                            {{ $participants }}
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            Verified Participants
                        </h5>
                    </div>
                    <div class="card-body">
                        <h1 class="card-text text-center">
                            {{ $verified }}
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            Unverified Participant
                        </h5>
                    </div>
                    <div class="card-body">
                        <h1 class="card-text text-center">
                            {{ $unverified }}
                        </h1>
                    </div>
                </div>
            </div>
        </div>

        {{-- Recent Register --}}
        <livewire:cms.participant />
    </div>
</div>
