<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>

    <h1 class="h3 mb-3">
        {{ $title ?? '' }}
    </h1>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">{{ $title ?? '' }} Data</h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <x-acc-header :isCreate="false">
                    <div class="col-md-6">
                        <div class="mt-3">
                            <label class="form-label fw-bold">Status</label>
                            <select wire:model="where_status" class="form-control">
                                <option value="all">All</option>
                                <option value="1">Sudah Bayar</option>
                                <option value="2">Ditolak</option>
                                <option value="0">Belum Bayar</option>
                            </select>
                        </div>
                    </div>
                </x-acc-header>
                <table class="table table-hover table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <x-acc-loop-th :searchBy="$searchBy" :orderBy="$orderBy" :order="$order" />
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($get as $d)
                            <tr>
                                <td>{{ $d->jenis_lari }}</td>
                                <td>{{ $d->grup }}</td>
                                <td>{{ $d->name }}</td>
                                <td>{{ $d->email }}</td>
                                <td>{{ $d->phone }}</td>
                                <td>{{ $d->gender }}</td>
                                <td>{{ $d->birth_date }}</td>
                                <td>{{ $d->address }}</td>
                                <td>{{ $d->distance }}</td>
                                <td>
                                    @php
                                        if($d->status == 1) {
                                            echo '<span class="text-success">Sudah Bayar</span>';
                                        } else if($d->status == 2) {
                                            echo '<span class="text-danger">Pembayaran Ditolak</span>';
                                        } else {
                                            echo '<span class="text-danger">Belum Bayar</span>';
                                        }
                                    @endphp
                                </td>
                                <td>{{ $d->number }}</td>
                                <td>{{ $d->order_id }}</td>
                                <td>{{ $d->created_at->format('d F Y') }}</td>
                                <td>
                                    <button
                                        class="btn btn-danger"
                                        wire:click="confirmDelete({{ $d->id }})"
                                    >
                                        <span wire:ignore>
                                            <i class="align-middle" data-feather="trash"></i>
                                            Delete
                                        </span>
                                    </button>
                                    {{-- <button
                                        class="btn btn-primary btn-sm"
                                        wire:click="checkPayment('{{ $d->order_id }}')"
                                    >
                                        Check Payment
                                    </button> --}}

                                    <button
                                        class="btn btn-primary btn-sm"
                                        wire:click="checkPaymentManual('{{ $d->id }}')"
                                        x-data
                                        @click="new bootstrap.Modal(document.getElementById('acc-modal')).show()"
                                    >
                                        Check Payment
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100" class="text-center">
                                    No Data Found
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                <div class="float-end">
                    {{ $get->links() }}
                </div>
            </div>
        </div>
    </div>

    <x-acc-modal title="Show Payment" id="acc-modal">
        <x-acc-form submit="">
            <h6>
                Unique number {{ $dada }}
            </h6>
            <img src="{{ asset('storage/events/' . $img ?? '') }}" alt="Payment">

            <x-slot name="actions">
                <button role="button"  wire:click="acc({{ $ids }})" class="btn btn-primary mb-3">Accept</button>

                <button role="button" wire:click="reject({{ $ids }})" class="btn btn-danger">Reject</button>
            </x-slot>
        </x-acc-form>
    </x-acc-modal>
</div>
