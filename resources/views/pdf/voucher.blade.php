<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Voucher</title>
        <style>
            /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */

            /*All the styling goes here*/

            img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%;
            }

            body {
                background-color: #f6f6f6;
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }

            table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%;
            }

            table td {
                font-family: sans-serif;
                font-size: 14px;
                vertical-align: top;
            }

            /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

            .body {
                background-color: #f6f6f6;
                width: 100%;
            }

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display: block;
                margin: 0 auto !important;
                /* makes it centered */
                max-width: 580px;
                padding: 10px;
                width: 580px;
            }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                box-sizing: border-box;
                display: block;
                margin: 0 auto;
                max-width: 580px;
                padding: 10px;
            }

            /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
            .main {
                background: #ffffff;
                border-radius: 3px;
                width: 100%;
            }

            .wrapper {
                box-sizing: border-box;
                padding: 20px;
            }

            .content-block {
                padding-bottom: 10px;
                padding-top: 10px;
            }

            .footer {
                clear: both;
                margin-top: 10px;
                text-align: center;
                width: 100%;
            }

            .footer td,
            .footer p,
            .footer span,
            .footer a {
                color: #999999;
                font-size: 12px;
                text-align: center;
            }

            /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                margin-bottom: 30px;
            }

            h1 {
                font-size: 35px;
                font-weight: 300;
                text-align: center;
                text-transform: capitalize;
            }

            p,
            ul,
            ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                margin-bottom: 15px;
            }

            p li,
            ul li,
            ol li {
                list-style-position: inside;
                margin-left: 5px;
            }

            a {
                color: #3498db;
                text-decoration: underline;
            }

            /* -------------------------------------
          BUTTONS
      ------------------------------------- */
            .btn {
                box-sizing: border-box;
                width: 100%;
            }

            .btn > tbody > tr > td {
                padding-bottom: 15px;
            }

            .btn table {
                width: auto;
            }

            .btn table td {
                background-color: #ffffff;
                border-radius: 5px;
                text-align: center;
            }

            .btn a {
                background-color: #ffffff;
                border: solid 1px #3498db;
                border-radius: 5px;
                box-sizing: border-box;
                color: #3498db;
                cursor: pointer;
                display: inline-block;
                font-size: 14px;
                font-weight: bold;
                margin: 0;
                padding: 12px 25px;
                text-decoration: none;
                text-transform: capitalize;
            }

            .btn-primary table td {
                background-color: #3498db;
            }

            .btn-primary a {
                background-color: #3498db;
                border-color: #3498db;
                color: #ffffff;
            }

            /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
            .last {
                margin-bottom: 0;
            }

            .first {
                margin-top: 0;
            }

            .align-center {
                text-align: center;
            }

            .align-right {
                text-align: right;
            }

            .align-left {
                text-align: left;
            }

            .clear {
                clear: both;
            }

            .mt0 {
                margin-top: 0;
            }

            .mb0 {
                margin-bottom: 0;
            }

            .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0;
            }

            .powered-by a {
                text-decoration: none;
            }

            hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                margin: 20px 0;
            }

            /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table.body h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                }

                table.body p,
                table.body ul,
                table.body ol,
                table.body td,
                table.body span,
                table.body a {
                    font-size: 16px !important;
                }

                table.body .wrapper,
                table.body .article {
                    padding: 10px !important;
                }

                table.body .content {
                    padding: 0 !important;
                }

                table.body .container {
                    padding: 0 !important;
                    width: 100% !important;
                }

                table.body .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                }

                table.body .btn table {
                    width: 100% !important;
                }

                table.body .btn a {
                    width: 100% !important;
                }

                table.body .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                }
            }

            /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%;
                }

                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                }

                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                }

                #MessageViewBody a {
                    color: inherit;
                    text-decoration: none;
                    font-size: inherit;
                    font-family: inherit;
                    font-weight: inherit;
                    line-height: inherit;
                }

                .btn-primary table td:hover {
                    background-color: #34495e !important;
                }

                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important;
                }
            }
        </style>
    </head>

    <body>
        <span class="preheader">{{ $subject }}</span>
        <table
            role="presentation"
            border="0"
            cellpadding="0"
            cellspacing="0"
            class="body"
        >
            <tr>
                <td>&nbsp;</td>
                <td class="container">
                    <div class="content">
                        <!-- START CENTERED WHITE CONTAINER -->
                        <table role="presentation" class="main">
                            <!-- START MAIN CONTENT AREA -->
                            <tr>
                                <td class="wrapper">
                                    <table
                                        role="presentation"
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                    >
                                        <tr>
                                            <td colspan="4">
                                                <img
                                                    src="{{ asset('logo.png') }}"
                                                    style="width: 100%"
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                colspan="4"
                                                style="text-align: center"
                                            >
                                                <h2>{!! $event->title !!}</h2>
                                                <h4>
                                                    {{
                                                    $event->start_date->isoFormat('dddd,
                                                    D MMMM Y') }}
                                                </h4>
                                                <p>
                                                    <br />
                                                    <br />
                                                    {{ $event->location }}
                                                </p>
                                                <p>
                                                    Pukul {{
                                                    $event->start_hour->format('H:i')
                                                    }}
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                colspan="4"
                                                style="text-align: center"
                                            >
                                                <p>
                                                    Nama :{{ $participants->name
                                                    }} <br />
                                                    Nomer Dada : {{
                                                    $final_number }}
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <span
                                                    style="text-align: center"
                                                >
                                                    <h2>
                                                        Syarat & Ketentuan
                                                        Pengambilan Race Pack
                                                        Peserta diwajibkan
                                                        untuk:
                                                    </h2>
                                                </span>
                                                <ol>
                                                    <li>
                                                        Sudah mendaftar dan
                                                        membayar biaya
                                                        pendaftaran sesuai
                                                        aturan.
                                                    </li>
                                                    <li>
                                                        Membawa lembar E-Ticket.
                                                    </li>
                                                    <li>
                                                        Menunjukkan kartu
                                                        identitas asli
                                                        (KTP/SIM/Kartu
                                                        Pelajar/KK) kepada
                                                        petugas saat mengambil
                                                        paket lomba.
                                                    </li>
                                                </ol>

                                                <span
                                                    style="text-align: center"
                                                >
                                                    <h2>
                                                        Syarat & Ketentuan
                                                        Pengambilan Race Pack
                                                        Peserta diwajibkan
                                                        untuk:
                                                    </h2>
                                                </span>
                                                <ol>
                                                    <li>
                                                        Tidak diperbolehkan
                                                        untuk mengganti ukuran
                                                        kaos lari saat mengambil
                                                        paket lomba, dengan
                                                        alasan apa pun.
                                                    </li>
                                                    <li>
                                                        Paket lomba (race pack)
                                                        tidak dapat diambil pada
                                                        hari perlombaan Buff Fun
                                                        Run 2023 dan panitia
                                                        penyelenggara tidak akan
                                                        menyediakan fasilitas
                                                        pengambilan paket lomba
                                                        di luar jadwal yang
                                                        telah ditetapkan dalam
                                                        Peraturan dan Pedoman
                                                        Buff Fun Run 2023.
                                                    </li>
                                                    <li>
                                                        Untuk menjaga keamanan
                                                        dan keselamatan pelari,
                                                        kepesertaan lomba dan
                                                        nomor lomba tidak dapat
                                                        dipindahtangankan atau
                                                        dijual kepada orang
                                                        lain.
                                                    </li>
                                                    <li>
                                                        Jika peserta diketahui
                                                        memindahtangankan nomor
                                                        lomba kepada orang lain,
                                                        maka mereka akan
                                                        didiskualifikasi dari
                                                        Buff Fun Run 2023 dan
                                                        tidak diperkenankan
                                                        untuk mendaftar atau
                                                        berpartisipasi di acara
                                                        tersebut.
                                                    </li>
                                                    <li>
                                                        Penggunaan nomor lomba
                                                        atas nama orang lain
                                                        juga akan mengakibatkan
                                                        diskualifikasi dan
                                                        larangan mendaftar serta
                                                        berpartisipasi di Buff
                                                        Fun Run 2023 Setiap
                                                        paket lomba (race pack)
                                                        akan berisi:
                                                        <ul>
                                                            <li>
                                                                BIB (Nomor Dada)
                                                            </li>
                                                            <li>Kaos lari</li>
                                                            <li>
                                                                Sponsor Produk
                                                            </li>
                                                            <li>
                                                                Medali (After
                                                                finish)
                                                            </li>
                                                            <li>
                                                                Refreshment
                                                                (After finish)
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ol>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <!-- END MAIN CONTENT AREA -->
                        </table>
                        <!-- END CENTERED WHITE CONTAINER -->

                        <!-- START FOOTER -->
                        <div class="footer">
                            <table
                                role="presentation"
                                border="0"
                                cellpadding="0"
                                cellspacing="0"
                            >
                                <tr>
                                    <td class="content-block powered-by">
                                        Powered by
                                        <a href="https://accsec.co.id/"
                                            >accsec.co.id</a
                                        >.
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- END FOOTER -->
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js" integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            const source = window.document.getElementsByTagName("body")[0];
            html2pdf().from(source).save();

        </script>
    </body>
</html>
