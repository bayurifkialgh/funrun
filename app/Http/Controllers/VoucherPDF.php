<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use App\Models\Event;
use App\Models\Website;

class VoucherPDF extends Controller
{
    public function index($order_id) {
        $participants = Participant::where('order_id', $order_id)->first();
        $event = Event::first();
        $setting = Website::first();
        $subject = 'BUFF FUN RUN 2023';

        // Final number
        $number = $participants->number;
        $base_number = '0000';

        $strlen = -strlen($number) + 1;
        $strlen = $strlen == 0 ? -1 : $strlen;

        $final_number = substr_replace($base_number, $number, $strlen);

        return view('pdf.voucher', compact(
            'participants',
            'event',
            'setting',
            'subject',
            'final_number',
        ));
    }
}
