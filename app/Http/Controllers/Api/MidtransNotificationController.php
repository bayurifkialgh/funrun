<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\SendVoucher;
use App\Traits\WithMidtrans;
use App\Models\Participant;
use Illuminate\Support\Facades\Mail;

class MidtransNotificationController extends Controller
{
    use WithMidtrans;

    public function notification() {
        $this->midtransInitialize();

        $midtransNotif = new \Midtrans\Notification;

        // If transaction is successful
        if(in_array($midtransNotif->transaction_status, [
            'accept',
            'settlement',
            'capture',
        ])) {
            $exe = Participant::where('order_id', $midtransNotif->order_id)->update([
                'status' => true,
                'signature_key' => $midtransNotif->signature_key,
                'payment_type' => $midtransNotif->payment_type,
                'settlement_time' => $midtransNotif->settlement_time,
            ]);

            // Get data from participant
            $exe = Participant::where('order_id', $midtransNotif->order_id)->first();


            // Set nomer dada
            $get_last_number = Participant::orderBy('number', 'desc')->where('distance', $exe->distance)->first();

            $start_with = $get_last_number->distance == '10k' ? 501 : 1;
            $number = $get_last_number->number ? $get_last_number->number + 1 : $start_with;
            $base_number = '0000';

            $strlen = -strlen($number) + 1;
            $strlen = $strlen == 0 ? -1 : $strlen;

            $final_number = substr_replace($base_number, $number, $strlen);

            // Update nomer
            Participant::where('order_id', $midtransNotif->order_id)->update([
                'number' => $number,
            ]);

            Mail::to($exe->email)->queue(new SendVoucher($exe, $final_number));
        }
    }
}
