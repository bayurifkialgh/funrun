<?php

namespace App\Mail;

use App\Models\Event;
use App\Models\Website;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendVoucher extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $participants;
    public $final_number;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($participants, $final_number)
    {
        $this->participants = $participants;
        $this->final_number = $final_number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $event = Event::first();
        $setting = Website::first();

        return $this->from(env('MAIL_USERNAME'), $setting->name)
            ->view('emails.voucher')
            ->with([
                'participants' => $this->participants,
                'event' => $event,
                'setting' => $setting,
                'subject' => 'BUFF FUN RUN 2023',
                'final_number' => $this->final_number,
            ]);
    }
}
