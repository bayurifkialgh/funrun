<?php

namespace App\Livewire\Web\Components;

use App\Models\Prize as ModelsPrize;
use Livewire\Component;

class Prize extends Component
{
    public $prize;

    public function mount() {
        $this->prize = ModelsPrize::orderBy('order', 'desc')->get();
    }

    public function render()
    {
        return view('livewire.web.components.prize');
    }
}
