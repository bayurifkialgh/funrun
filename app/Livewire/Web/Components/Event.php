<?php

namespace App\Livewire\Web\Components;

use Livewire\Component;

class Event extends Component
{
    public $event;
    public $settings;

    public function mount($event, $settings)
    {
        $this->settings = $settings;
        $this->event = $event;
    }

    public function render()
    {
        return view('livewire.web.components.event');
    }
}
