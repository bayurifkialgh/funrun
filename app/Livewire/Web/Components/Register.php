<?php

namespace App\Livewire\Web\Components;

use App\Models\Participant;
use App\Traits\WithMidtrans;
use App\Traits\WithSaveFile;
use Livewire\Component;
use Livewire\WithFileUploads;

class Register extends Component
{
    use WithMidtrans, WithFileUploads, WithSaveFile;

    public $name,
        $email,
        $phone,
        $gender,
        $birth_date,
        $address,
        $distance,
        $snapToken,
        $jenis_lari,
        $grup,
        $syarat,
        $number,
        $final_number,
        $file;

    public $recent;

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required|min:10|max:13',
        'gender' => 'required|in:L,P',
        'birth_date' => 'required|date',
        'address' => 'required',
        'distance' => 'required|in:5k,10k',
        'jenis_lari' => 'required',
        'syarat' => 'required',
    ];

    public function mount() {
        if(session('participant')) {
            $this->name = session('participant')->name;
            $this->email = session('participant')->email;
            $this->phone = session('participant')->phone;
            $this->gender = session('participant')->gender == 'male' ? 'L' : 'P';
            $this->birth_date = session('participant')->birth_date;
            $this->address = session('participant')->address;
            $this->distance = session('participant')->distance;
            $this->snapToken = session('participant')->snap_token;
            $this->jenis_lari = session('participant')->jenis_lari;
            $this->grup = session('participant')->grup;
            $this->number = session('participant')->number;
            $this->final_number = session('final_number');

            $this->recent = Participant::find(session('participant')->id);
        }
    }

    public function render()
    {
        return view('livewire.web.components.register');
    }

    public function new() {
        session()->pull('participant');
        session()->pull('final_number');

        $this->reset();
    }

    public function save() {
        $this->validate();

        $order_id = uniqid();

        // $this->midtrans($order_id);
        $this->saveParticipant($order_id);
    }

    public function saveParticipant($order_id) {

        // Set nomer dada
        $get_last_number = Participant::orderBy('number', 'desc')->where('distance', $this->distance)->first();

        $base_number = '0000';
        $number = $this->distance == '10k' ? 501 : 1;

        if($get_last_number) {
            $number = $get_last_number->number ? $get_last_number->number + 1 : $number;
        }

        $strlen = -strlen($number) + 1;
        $strlen = $strlen == 0 ? -1 : $strlen;

        $final_number = substr_replace($base_number, $number, $strlen);
        $this->final_number = $final_number;

        $exe = Participant::create([
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'gender' => $this->gender == 'L' ? 'male' : 'female',
            'birth_date' => $this->birth_date,
            'address' => $this->address,
            'distance' => $this->distance,
            'snap_token' => $this->snapToken,
            'status' => false,
            'order_id' => $order_id,
            'jenis_lari' => $this->jenis_lari,
            'grup' => $this->jenis_lari == 'Grup' ? $this->grup : null,
            'number' => $number,
        ]);

        $this->recent = $exe;

        session([
            'participant' => $exe,
            'final_number' => $final_number,
        ]);
    }

    public function midtrans($order_id) {
        $customerDetails = [
            'first_name' => $this->name,
            'last_name' => ' ',
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
        ];

        $transactionDetails = [
            'order_id' => $order_id,
            'gross_amount' => $this->distance == '5k' ? 200000 : 300000,
        ];

        $payload = [
            'transaction_details' => $transactionDetails,
            'customer_details' => $customerDetails
        ];

        $this->getSnapToken($payload);

        $this->emit('payment', $this->snapToken);
    }

    public function upload() {
        $this->validate([
            'file' => 'required|image|max:2048'
        ]);

        $old = Participant::find(session('participant')->id);

        $save_path = 'events';

        // Save image
        if($this->file) {
            $this->file = $this->saveFile($this->file, $save_path, $save_path)['filename'];
        } else {
            $this->file = $old->file;
        }

        // Save
        $old->update([
            'bukti_pembayaran' => $this->file
        ]);

        $this->recent = Participant::find(session('participant')->id);

        $this->emit('alert', [
            'type' => 'success',
            'message' => 'Berhasil upload file',
        ]);

        $this->emit('closeModal', 'modal-bukti-pembayaran');
    }
}
