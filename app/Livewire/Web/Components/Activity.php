<?php

namespace App\Livewire\Web\Components;

use App\Models\EventActivity;
use Livewire\Component;

class Activity extends Component
{
    public $activities;

    public function mount() {
        $this->activities = EventActivity::orderBy('order', 'asc')->get();
    }

    public function render()
    {
        return view('livewire.web.components.activity');
    }
}
