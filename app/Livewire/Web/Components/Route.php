<?php

namespace App\Livewire\Web\Components;

use Livewire\Component;

class Route extends Component
{
    public $event;

    public function mount($event) {
        $this->event = $event;
    }

    public function render()
    {
        return view('livewire.web.components.route');
    }
}
