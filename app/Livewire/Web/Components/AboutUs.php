<?php

namespace App\Livewire\Web\Components;

use Livewire\Component;

class AboutUs extends Component
{
    public $settings;

    public function mount($settings) {
        $this->settings = $settings;
    }

    public function render()
    {
        return view('livewire.web.components.about-us');
    }
}
