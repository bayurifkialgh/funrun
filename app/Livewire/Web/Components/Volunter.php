<?php

namespace App\Livewire\Web\Components;

use App\Models\Volunter as ModelsVolunter;
use Livewire\Component;

class Volunter extends Component
{
    public $name;
    public $email;
    public $phone;

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Please enter name',
        'email.required' => 'Please enter email',
        'email.email' => 'Please enter valid email',
        'phone.required' => 'Please enter phone',
    ];

    public function mount() {
        if(session('volunter')) {
            $this->name = session('volunter')->name;
            $this->email = session('volunter')->email;
            $this->phone = session('volunter')->phone;
        }
    }

    public function render()
    {
        return view('livewire.web.components.volunter');
    }

    public function save() {
        $this->validate();

        $exe = ModelsVolunter::create([
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone
        ]);

        $this->emit('alert', [
            'type' => 'success',
            'message' => 'Volunter saved'
        ]);

        session([
            'volunter' => $exe
        ]);
    }

    public function new() {
        session()->pull('volunter');

        $this->reset();
    }
}
