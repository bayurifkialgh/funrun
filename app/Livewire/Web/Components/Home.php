<?php

namespace App\Livewire\Web\Components;

use Livewire\Component;

class Home extends Component
{
    public $event;

    public function mount($event) {
        $this->event = $event;
    }

    public function render()
    {
        return view('livewire.web.components.home');
    }
}
