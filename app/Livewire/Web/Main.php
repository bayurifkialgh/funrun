<?php

namespace App\Livewire\Web;

use App\Models\Event;
use App\Models\Website;
use Livewire\Component;

class Main extends Component
{
    public $event;
    public $settings;

    public function mount() {
        $this->event = Event::first();
        $this->settings = Website::first();
        $this->settings->opengraph = json_decode($this->settings->opengraph, true);
        $this->settings->dulbincore = json_decode($this->settings->dulbincore, true);
        $this->settings->social_media = json_decode($this->settings->social_media, true);
    }

    public function render()
    {
        return view('livewire.web.main');
    }
}
