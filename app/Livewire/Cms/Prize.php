<?php

namespace App\Livewire\Cms;

use App\Models\Prize as ModelsPrize;
use App\Traits\WithChangeOrder;
use App\Traits\WithCreateAction;
use App\Traits\WithDeleteAction;
use App\Traits\WithEditAction;
use App\Traits\WithGetFilterData;
use App\Traits\WithSaveAction;
use App\Traits\WithSaveFile;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Prize extends Component
{
    use WithChangeOrder, WithCreateAction, WithDeleteAction, WithEditAction, WithSaveAction, WithPagination, WithGetFilterData, WithFileUploads, WithSaveFile;

    protected $listeners = ['delete' => 'delete'];
    protected $paginationTheme = 'bootstrap';
    public $title = 'Door Prize';
    public $searchBy = [
        [
            'name' => 'Order',
            'field' => 'order',
        ],
        [
            'name' => 'Name',
            'field' => 'name',
        ],
        [
            'name' => 'Price',
            'field' => 'price',
        ],
        [
            'name' => 'Image',
            'field' => 'image',
            'no_search' => true,
        ]
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'order',
    $order = 'asc';

    public $old_img;
    public $m_id,
        $m_order,
        $m_name,
        $m_price,
        $m_image;

    protected $rules = [
        'm_order' => 'required',
        'm_name' => 'required',
        'm_price' => 'required',
        'm_image' => 'nullable|image|max:2048',
    ];

    protected $messages = [
        'm_order.required' => 'Order is required',
        'm_name.required' => 'Name is required',
        'm_price.required' => 'Price is required',
        'm_image.image' => 'Image must be an image',
        'm_image.max' => 'Image must be less than 2MB',
    ];

    public function render()
    {
        $get = $this->getDataWithFilter(new ModelsPrize, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }

        return view('livewire.cms.prize', compact('get'))->layout('components.cms');
    }

    public function getDetail($id) {
        $get = ModelsPrize::find($id);

        $this->m_id = $get->id;
        $this->m_order = $get->order;
        $this->m_name = $get->name;
        $this->m_price = $get->price;
        $this->old_img = $get->image;
        $this->isUpdate = true;
    }

    public function saveAction() {
        $this->validate();

        $save_path = 'prizes';

        // Save image
        if($this->m_image) {
            $this->m_image = $this->saveFile($this->m_image, $save_path, $save_path)['filename'];
        } else {
            $this->m_image = $this->old_img;
        }

        if($this->isUpdate) {
            ModelsPrize::find($this->m_id)->update([
                'order' => $this->m_order,
                'name' => $this->m_name,
                'price' => $this->m_price,
                'image' => $this->m_image,
            ]);
        } else {
            ModelsPrize::create([
                'order' => $this->m_order,
                'name' => $this->m_name,
                'price' => $this->m_price,
                'image' => $this->m_image,
            ]);
        }
    }

    public function deleteAction($id) {
        ModelsPrize::find($id)->delete();
    }
}
