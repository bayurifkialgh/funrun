<?php

namespace App\Livewire\Cms;

use App\Models\Event;
use App\Models\EventActivity;
use App\Traits\WithChangeOrder;
use App\Traits\WithCreateAction;
use App\Traits\WithDeleteAction;
use App\Traits\WithEditAction;
use App\Traits\WithGetFilterData;
use App\Traits\WithSaveAction;
use Livewire\Component;
use Livewire\WithPagination;

class Activity extends Component
{
    use WithChangeOrder, WithCreateAction, WithDeleteAction, WithEditAction, WithSaveAction, WithPagination, WithGetFilterData;

    protected $listeners = ['delete' => 'delete'];
    protected $paginationTheme = 'bootstrap';
    public $title = 'Event Activity';
    public $searchBy = [
        [
            'name' => 'Order',
            'field' => 'order',
        ],
        [
            'name' => 'Title',
            'field' => 'title',
        ],
        [
            'name' => 'Description',
            'field' => 'description',
        ]
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'order',
    $order = 'asc';

    public $m_id,
        $m_order,
        $m_title,
        $m_description;

    protected $rules = [
        'm_order' => 'required',
        'm_title' => 'required',
        'm_description' => 'nullable',
    ];

    protected $messages = [
        'm_order.required' => 'Order is required',
        'm_title.required' => 'Title is required',
    ];

    public function render()
    {
        $get = $this->getDataWithFilter(new EventActivity, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }

        return view('livewire.cms.activity', compact('get'))->layout('components.cms');
    }

    public function getDetail($id) {
        $get = EventActivity::find($id);

        $this->m_id = $get->id;
        $this->m_order = $get->order;
        $this->m_title = $get->title;
        $this->m_description = $get->description;
    }

    public function saveAction() {
        $this->validate();

        if($this->isUpdate) {
            EventActivity::find($this->m_id)->update([
                'order' => $this->m_order,
                'title' => $this->m_title,
                'description' => $this->m_description,
            ]);
        } else {
            EventActivity::create([
                'event_id' => Event::first()->id,
                'order' => $this->m_order,
                'title' => $this->m_title,
                'description' => $this->m_description,
            ]);
        }
    }

    public function deleteAction($id) {
        EventActivity::find($id)->delete();
    }
}
