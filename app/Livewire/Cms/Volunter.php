<?php

namespace App\Livewire\Cms;

use App\Models\Volunter as ModelsVolunter;
use App\Traits\WithChangeOrder;
use App\Traits\WithDeleteAction;
use App\Traits\WithGetFilterData;
use App\Traits\WithMidtrans;
use Livewire\Component;
use Livewire\WithPagination;

class Volunter extends Component
{
    use WithChangeOrder, WithPagination, WithDeleteAction, WithGetFilterData, WithMidtrans;

    protected $listeners = ['delete' => 'delete'];
    protected $paginationTheme = 'bootstrap';
    public $title = 'Participant';
    public $where_status = 'all';
    public $searchBy = [
        [
            'name' => 'Name',
            'field' => 'name',
        ],
        [
            'name' => 'Email',
            'field' => 'email',
        ],
        [
            'name' => 'Phone',
            'field' => 'phone',
        ],
        [
            'name' => 'Created at',
            'field' => 'created_at',
            'no_search' => true,
        ],
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'created_at',
    $order = 'desc';

    public function render()
    {
        $get = $this->getDataWithFilter(new ModelsVolunter, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }

        return view('livewire.cms.volunter', compact('get'))->layout('components.cms');
    }

    public function deleteAction($id) {
        ModelsVolunter::find($id)->delete();
    }
}
