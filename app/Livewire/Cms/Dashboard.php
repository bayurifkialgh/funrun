<?php

namespace App\Livewire\Cms;

use App\Models\Participant;
use Livewire\Component;

class Dashboard extends Component
{
    public $title = 'Dashboard';
    public $participants;
    public $verified;
    public $unverified;

    public function mount() {
        $this->participants = Participant::count();
        $this->verified = Participant::where('status', 1)->count();
        $this->unverified = Participant::where('status', 0)->count();
    }

    public function render()
    {
        return view('livewire.cms.dashboard')->layout('components.cms');
    }
}
