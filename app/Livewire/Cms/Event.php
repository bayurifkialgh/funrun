<?php

namespace App\Livewire\Cms;

use App\Models\Event as ModelsEvent;
use App\Traits\WithSaveAction;
use App\Traits\WithSaveFile;
use Livewire\Component;
use Livewire\WithFileUploads;

class Event extends Component
{
    use WithSaveAction, WithFileUploads, WithSaveFile;

    public $title = 'Event';
    public $data;
    public $isUpdate = true,
        $m_title,
        $m_subtitle,
        $m_main_description,
        $m_registration_description,
        $m_route,
        $m_route_description,
        $m_activities_description,
        $m_registration_link,
        $m_event_details,
        $m_start_date,
        $m_start_hour,
        $m_end_hour,
        $m_price_total,
        $m_location,
        $m_map,
        $m_map2;

    protected $rules = [
        'm_title' => 'required',
        'm_subtitle' => 'required',
        'm_main_description' => 'required',
        'm_registration_description' => 'required',
        'm_registration_link' => 'required',
        // 'm_route' => 'required',
        // 'm_route_description' => 'required',
        // 'm_activities_description' => 'required',
        // 'm_event_details' => 'required',
        'm_start_date' => 'required',
        'm_start_hour' => 'required',
        'm_end_hour' => 'required',
        'm_price_total' => 'required',
        'm_location' => 'required',
    ];

    protected $messages = [
        'm_title.required' => 'Title is required',
        'm_subtitle.required' => 'Subtitle is required',
        'm_main_description.required' => 'Main Description is required',
        'm_registration_description.required' => 'Registration Button is required',
        'm_registration_link.required' => 'Registration Link is required',
        // 'm_route.required' => 'Route is required',
        // 'm_route_description.required' => 'Route Description is required',
        // 'm_activities_description.required' => 'Activities Description is required',
        // 'm_event_details.required' => 'Event Details is required',
        'm_start_date.required' => 'Start Date is required',
        'm_start_hour.required' => 'Start Hour is required',
        'm_end_hour.required' => 'End Hour is required',
        'm_price_total.required' => 'Price Total is required',
        'm_location.required' => 'Location is required',
    ];

    public function mount() {
        $this->data = ModelsEvent::first();
        $this->m_title = $this->data->title;
        $this->m_subtitle = $this->data->subtitle;
        $this->m_main_description = $this->data->main_description;
        $this->m_registration_description = $this->data->registration_description;
        $this->m_route = $this->data->route;
        $this->m_route_description = $this->data->route_description;
        $this->m_activities_description = $this->data->activities_description;
        $this->m_registration_link = $this->data->registration_link;
        $this->m_event_details = $this->data->event_details;
        $this->m_start_date = $this->data->start_date->format('Y-m-d');
        $this->m_start_hour = $this->data->start_hour->format('H:i');
        $this->m_end_hour = $this->data->end_hour->format('H:i');
        $this->m_price_total = $this->data->price_total;
        $this->m_location = $this->data->location;
        $this->m_map = null;
        $this->m_map2 = null;
    }

    public function render()
    {
        return view('livewire.cms.event')->layout('components.cms');
    }

    public function saveAction() {
        $this->validate();

        $save_path = 'events';

        // Save image
        if($this->m_map) {
            $this->m_map = $this->saveFile($this->m_map, $save_path, $save_path)['filename'];
        } else {
            $this->m_map = $this->data->map;
        }

        // Save image
        if($this->m_map2) {
            $this->m_map2 = $this->saveFile($this->m_map2, $save_path, $save_path)['filename'];
        } else {
            $this->m_map2 = $this->data->map2;
        }

        ModelsEvent::first()->update([
            'title' => $this->m_title,
            'subtitle' => $this->m_subtitle,
            'main_description' => $this->m_main_description,
            'registration_description' => $this->m_registration_description,
            'route' => $this->m_route,
            'route_description' => $this->m_route_description,
            'activities_description' => $this->m_activities_description,
            'registration_link' => $this->m_registration_link,
            'event_details' => $this->m_event_details,
            'start_date' => $this->m_start_date,
            'start_hour' => $this->m_start_hour,
            'end_hour' => $this->m_end_hour,
            'price_total' => $this->m_price_total,
            'location' => $this->m_location,
            'map' => $this->m_map,
            'map2' => $this->m_map2,
        ]);

        $this->data = ModelsEvent::first();
        $this->m_map = null;
        $this->m_map2 = null;
    }
}
