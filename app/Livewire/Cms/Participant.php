<?php

namespace App\Livewire\Cms;

use App\Enums\Alert;
use App\Mail\SendVoucher;
use Illuminate\Support\Facades\Mail;
use App\Models\Participant as ModelsParticipant;
use App\Traits\WithChangeOrder;
use App\Traits\WithDeleteAction;
use App\Traits\WithGetFilterData;
use App\Traits\WithMidtrans;
use Livewire\Component;
use Livewire\WithPagination;

class Participant extends Component
{
    use WithChangeOrder, WithPagination, WithDeleteAction, WithGetFilterData, WithMidtrans;

    protected $listeners = [
        'delete' => 'delete',
        'accAction' => 'accAction',
        'rejectAction' => 'rejectAction',
    ];
    protected $paginationTheme = 'bootstrap';
    public $title = 'Participant';
    public $where_status = 'all';
    public $searchBy = [
        [
            'name' => 'Jenis Lari',
            'field' => 'jenis_lari',
        ],
        [
            'name' => 'Grup',
            'field' => 'grup',
        ],
        [
            'name' => 'Name',
            'field' => 'name',
        ],
        [
            'name' => 'Email',
            'field' => 'email',
        ],
        [
            'name' => 'Phone',
            'field' => 'phone',
        ],
        [
            'name' => 'Gender',
            'field' => 'gender',
        ],
        [
            'name' => 'Birth Date',
            'field' => 'birth_date',
        ],
        [
            'name' => 'Address',
            'field' => 'address',
        ],
        [
            'name' => 'Distance',
            'field' => 'distance',
        ],
        [
            'name' => 'Status',
            'field' => 'status',
        ],
        [
            'name' => 'Number',
            'field' => 'number',
        ],
        [
            'name' => 'Order ID',
            'field' => 'order_id',
        ],
        [
            'name' => 'Created at',
            'field' => 'created_at',
            'no_search' => true,
        ],
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'created_at',
    $order = 'desc';

    public $img, $ids, $dada;

    public function render()
    {
        $model = new ModelsParticipant;

        if($this->where_status != 'all') {
            $model =  $model->where('status', $this->where_status);
        }

        $get = $this->getDataWithFilter($model, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }

        return view('livewire.cms.participant', compact('get'))->layout('components.cms');
    }

    public function deleteAction($id) {
        ModelsParticipant::find($id)->delete();
    }

    public function checkPaymentManual($id) {
        $data = ModelsParticipant::find($id);

        $this->ids = $id;
        $this->dada = $data->number;
        $this->img = $data->bukti_pembayaran;
    }

    public function acc($id) {
        $this->emit('confirm', [
            'id' => $id,
            'function' => 'accAction'
        ]);
    }

    public function reject($id) {
        $this->emit('confirm', [
            'id' => $id,
            'function' => 'rejectAction'
        ]);
    }

    public function accAction($id) {
        ModelsParticipant::find($id)->update([
            'status' => true,
        ]);

        $exe = ModelsParticipant::find($id);
        $number = $exe->number;
        $base_number = '0000';

        $strlen = -strlen($number) + 1;
        $strlen = $strlen == 0 ? -1 : $strlen;

        $final_number = substr_replace($base_number, $number, $strlen);

        Mail::to($exe->email)->queue(new SendVoucher($exe, $final_number));

        $this->emit('alert', [
            'type' => Alert::success,
            'message' => 'Data accepted',
        ]);
        $this->emit('closeModal' ,'acc-modal');
    }

    public function rejectAction($id) {
        ModelsParticipant::find($id)->update([
            'status' => 2,
        ]);

        $this->emit('alert', [
            'type' => Alert::success,
            'message' => 'Data rejected',
        ]);
        $this->emit('closeModal' ,'acc-modal');
    }

    public function checkPayment($order_id) {
        $this->midtransInitialize();

        try {

            $status = \Midtrans\Transaction::status($order_id);

            $type = Alert::warning;

            if(in_array($status->transaction_status, [
                'accept',
                'settlement',
                'capture',
            ])) {
                $type = Alert::success;

                ModelsParticipant::where('order_id', $order_id)->update([
                    'status' => true,
                ]);
            }
            $this->emit('alert', [
                'type' => $type,
                'message' => ucfirst($status->transaction_status),
            ]);
        } catch (\Exception $e) {
            $this->emit('alert', [
                'type' => Alert::warning,
                'message' => 'User not choose payment method yet',
            ]);
        }
    }
}
