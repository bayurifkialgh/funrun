<?php

namespace App\Livewire\Cms;

use App\Models\Website as ModelsWebsite;
use Livewire\WithFileUploads;
use App\Traits\WithSaveFile;
use App\Traits\WithSaveAction;
use Livewire\Component;

class Website extends Component
{
    use WithFileUploads, WithSaveFile, WithSaveAction;

    public $title = 'Website';
    public $data;
    public $isUpdate = true,
        $m_name,
        $m_logo,
        $m_favicon,
        $m_email,
        $m_phone,
        $m_address,
        $m_author,
        $m_description,
        $m_keywords,
        $m_opengraph = [
            'site_name' => '',
            'title' => '',
            'description' => '',
            'type' => '',
            'url' => '',
            'image' => '',
        ],
        $m_dulbincore = [
            'title' => '',
            'description' => '',
            'publisher' => '',
            'publisher_url' => '',
            'creator_name' => '',
            'language' => '',
            'subject' => '',
        ],
        $google_analytics,
        $social_media,
        $opengraph_image,
        $about_us;

    protected $rules = [
        'm_name' => 'required',
        'm_logo' => 'nullable|image:jpeg,png,jpg,svg|max:2048',
        'm_favicon' => 'nullable|image:jpeg,png,jpg,svg|max:2048',
        'm_email' => 'required',
        'm_phone' => 'required',
        'm_address' => 'required',
        'm_author' => 'required',
        'm_description' => 'required',
        'm_keywords' => 'required',
        'm_opengraph' => 'required',
        'm_opengraph.site_name' => 'required',
        'm_opengraph.title' => 'required',
        'm_opengraph.description' => 'required',
        'm_opengraph.type' => 'required',
        'm_opengraph.url' => 'required',
        'm_opengraph.image' => 'nullable|image:jpeg,png,jpg,svg|max:2048',
        'm_dulbincore' => 'required',
        'm_dulbincore.title' => 'required',
        'm_dulbincore.description' => 'required',
        'm_dulbincore.publisher' => 'required',
        'm_dulbincore.publisher_url' => 'required',
        'm_dulbincore.creator_name' => 'required',
        'm_dulbincore.language' => 'required',
        'm_dulbincore.subject' => 'required',
        'google_analytics' => 'required',
        'social_media' => 'required',
        'about_us' => 'required',
    ];

    protected $messages = [
        'm_name.required' => 'Website name is required',
        'm_logo.image' => 'Logo is required',
        'm_favicon.image' => 'Favicon is required',
        'm_email.required' => 'Email is required',
        'm_phone.required' => 'Phone is required',
        'm_address.required' => 'Instagram is required',
        'm_author.required' => 'Author is required',
        'm_description.required' => 'Description is required',
        'm_keywords.required' => 'Keywords is required',
        'm_opengraph.required' => 'Open Graph is required',
        'm_opengraph.site_name.required' => 'Site Name is required',
        'm_opengraph.title.required' => 'Title is required',
        'm_opengraph.description.required' => 'Description is required',
        'm_opengraph.type.required' => 'Type is required',
        'm_opengraph.url.required' => 'URL is required',
        'm_opengraph.image.required' => 'Image is required',
        'm_dulbincore.required' => 'Dulbin Core is required',
        'm_dulbincore.required' => 'Dulbin Core is required',
        'm_dulbincore.title.required' => 'Title is required',
        'm_dulbincore.description.required' => 'Description is required',
        'm_dulbincore.publisher.required' => 'Publisher is required',
        'm_dulbincore.publisher_url.required' => 'Publisher URL is required',
        'm_dulbincore.creator_name.required' => 'Creator Name is required',
        'm_dulbincore.language.required' => 'Language is required',
        'm_dulbincore.subject.required' => 'Subject is required',
        'google_analytics.required' => 'Google Analytics is required',
        'social_media.required' => 'Social Media is required',
        'about_us.required' => 'About Us is required',
    ];

    public function mount() {
        $this->data = ModelsWebsite::first();
        $this->m_name = $this->data->name;
        // $this->m_logo = $this->data->logo;
        // $this->m_favicon = $this->data->favicon;
        $this->m_email = $this->data->email;
        $this->m_phone = $this->data->phone;
        $this->m_address = $this->data->address;
        $this->m_author = $this->data->author;
        $this->m_description = $this->data->description;
        $this->m_keywords = $this->data->keywords;
        $this->about_us = $this->data->about_us;
        $this->m_opengraph = json_decode($this->data->opengraph, true);
        $this->m_dulbincore = json_decode($this->data->dulbincore, true);
        $this->google_analytics = $this->data->google_analytics;
        $this->social_media = json_decode($this->data->social_media, true);
        $this->opengraph_image = $this->m_opengraph['image'];
        $this->m_opengraph['image'] = null;
    }

    public function render()
    {
        return view('livewire.cms.website')->layout('components.cms');
    }

    public function saveAction() {
        $this->validate();

        $save_path = 'settings';

        // Save image
        if($this->m_logo) {
            $this->m_logo = $this->saveFile($this->m_logo, $save_path, $save_path)['filename'];
        } else {
            $this->m_logo = $this->data->logo;
        }

        if($this->m_favicon) {
            $this->m_favicon = $this->saveFile($this->m_favicon, $save_path, $save_path)['filename'];
        } else {
            $this->m_favicon = $this->data->favicon;
        }

        if($this->m_opengraph['image']) {
            $this->m_opengraph['image'] = $this->saveFile($this->m_opengraph['image'], $save_path, $save_path)['filename'];
        } else {
            $this->m_opengraph['image'] = $this->data->opengraph_image;
        }


        $this->opengraph_image = $this->m_opengraph['image'];

        ModelsWebsite::first()->update([
            'name' => $this->m_name,
            'email' => $this->m_email,
            'phone' => $this->m_phone,
            'address' => $this->m_address,
            'author' => $this->m_author,
            'description' => $this->m_description,
            'keywords' => $this->m_keywords,
            'opengraph' => json_encode($this->m_opengraph),
            'dulbincore' => json_encode($this->m_dulbincore),
            'google_analytics' => $this->google_analytics,
            'social_media' => json_encode($this->social_media),
            'logo' => $this->m_logo,
            'favicon' => $this->m_favicon,
            'about_us' => $this->about_us,
        ]);

        $this->m_opengraph['image'] = null;
        $this->m_logo = null;
        $this->m_favicon = null;
    }
}
