<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'gender',
        'birth_date',
        'address',
        'distance',
        'status',
        'snap_token',
        'order_id',
        'signature_key',
        'payment_type',
        'settlement_time',
        'number',
        'jenis_lari',
        'grup',
        'bukti_pembayaran',
    ];
}
