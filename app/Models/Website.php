<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    use HasFactory;

    protected $table = 'website';

    protected $fillable = [
        'name',
        'logo',
        'favicon',
        'email',
        'phone',
        'address',
        'description',
        'author',
        'keywords',
        'opengraph',
        'dulbincore',
        'google_analytics',
        'social_media',
        'privacy_policy',
        'term_of_service',
        'about_us',
    ];
}
