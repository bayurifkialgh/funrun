<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volunter extends Model
{
    use HasFactory;

    protected $table = 'volunteer';

    protected $fillable = [
        'name',
        'email',
        'phone',
    ];
}
