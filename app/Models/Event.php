<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $table = 'event';

    protected $fillable = [
        'title',
        'subtitle',
        'main_description',
        'registration_description',
        'route',
        'route_description',
        'activities_description',
        'registration_link',
        'event_details',
        'start_date',
        'location',
        'start_hour',
        'end_hour',
        'price_total',
        'map',
        'map2',
    ];


    protected $casts = [
        'start_date' => 'datetime',
        'start_hour' => 'datetime',
        'end_hour' => 'datetime'
    ];

}
