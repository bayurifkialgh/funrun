<?php

namespace App\Traits;

use App\Enums\Alert;

trait WithDeleteAction {

    public function confirmDelete($id) {
        $this->emit('confirm', [
            'id' => $id,
            'function' => 'delete'
        ]);
    }

    public function delete($id) {
        $this->deleteAction($id);

        $this->emit('alert', [
            'type' => Alert::success,
            'message' => 'Data deleted successfully',
        ]);
    }
}
