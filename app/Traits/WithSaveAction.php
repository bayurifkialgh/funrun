<?php

namespace App\Traits;

use App\Enums\Alert;

trait WithSaveAction {
    public function save() {
        $this->saveAction();

        $this->emit('closeModal', 'acc-modal');
        $this->emit('alert', [
            'type' => Alert::success,
            'message' => $this->isUpdate ? 'Data Updated' : 'Data Created'
        ]);
    }
}
